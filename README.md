# README #

With blitz++ being unmaintained, I decided to work on a new template
library with similar capabilities. At this point, it is mostly a pet
project. Ideally, donner++ will have most of the useful features blitz++
provides with low overhead and good performance in general. However, I
want the code to be leaner and more modern, building on C++11 language
and library features.

### What is this repository for? ###

Development of donner++.

### How do I get set up? ###

Include donner.h.

### Licensing ###

Either LGPLv3 (or later) or GPLv2 (or later).

### Contribution guidelines ###

It is just a pet project currently.

### Who do I talk to? ###

Andreas Kempf (aakempf gmail com)
(Working/studying at the German Ruhr-Universität Bochum)
