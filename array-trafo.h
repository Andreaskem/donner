/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_ARRAY_TRAFO_H
#define DONNER_ARRAY_TRAFO_H

#include "donner.h"

namespace donner {

template<typename DataType, int Dimensionality>
class ArrayReal {
	public:
		typedef DataType DT;
		static const int Dim = Dimensionality;
		typedef TinyVector<int, Dim> Index;

	private:
		typedef Array<std::complex<DT>, Dim> CArray;
		CArray& op;

	public:
		constexpr ArrayReal(CArray& op) : op(op) {}

		void operator=(const DT& rhs) {
			Index ind = op.getStart();
			do {
				op(ind).real(rhs);
			} while(next_index(op.getStart(), op.getStop(), ind));
		}

		void operator=(const Array<DT, Dim>& rhs) {
			Index ind = op.getStart();
			do {
				op(ind).real(rhs(ind));
			} while(next_index(op.getStart(), op.getStop(), ind));
		}

		constexpr DT operator()(const Index ind) const {
			return(op(ind).real());
		}

		constexpr Index getStart() const {
			return(op.getStart());
		}

		constexpr int getStart(int i) const {
			return(op.getStart(i));
		}

		constexpr Index getStop() const {
			return(op.getStop());
		}

		constexpr int getStop(int i) const {
			return(op.getStop(i));
		}
};

template<typename DataType, int Dimensionality>
struct is_arr<ArrayReal<DataType, Dimensionality>> {
	  static const bool value = true;
};

template<typename DataType, int Dimensionality>
constexpr ArrayReal<DataType, Dimensionality> real(Array<std::complex<DataType>, Dimensionality>& arr) {
	return(ArrayReal<DataType, Dimensionality>(arr));
}

template<typename DataType, int Dimensionality>
class ArrayImag {
	public:
		typedef DataType DT;
		static const int Dim = Dimensionality;
		typedef TinyVector<int, Dim> Index;

	private:
		typedef Array<std::complex<DT>, Dim> CArray;
		CArray& op;

	public:
		constexpr ArrayImag(CArray& op) : op(op) {}

		void operator=(const DT& rhs) {
			Index ind = op.getStart();
			do {
				op(ind).imag(rhs);
			} while(next_index(op.getStart(), op.getStop(), ind));
		}

		void operator=(const Array<DT, Dim>& rhs) {
			Index ind = op.getStart();
			do {
				op(ind).imag(rhs(ind));
			} while(next_index(op.getStart(), op.getStop(), ind));
		}

		constexpr DT operator()(const Index ind) const {
			return(op(ind).imag());
		}

		constexpr Index getStart() const {
			return(op.getStart());
		}

		constexpr int getStart(int i) const {
			return(op.getStart(i));
		}

		constexpr Index getStop() const {
			return(op.getStop());
		}

		constexpr int getStop(int i) const {
			return(op.getStop(i));
		}
};

template<typename DataType, int Dimensionality>
struct is_arr<ArrayImag<DataType, Dimensionality>> {
	  static const bool value = true;
};

template<typename DataType, int Dimensionality>
constexpr ArrayImag<DataType, Dimensionality> imag(Array<std::complex<DataType>, Dimensionality>& arr) {
	return(ArrayImag<DataType, Dimensionality>(arr));
}

template<typename DataType, int Dimensionality, int Components>
class ArrayComp {
	public:
		typedef DataType DT;
		static const int Dim = Dimensionality;
		typedef TinyVector<int, Dim> Index;

	private:
		typedef Array<TinyVector<DT, Components>, Dim> VArray;
		VArray& op;
		const int component;

	public:
		constexpr ArrayComp(VArray& op, int component) : op(op), component(component) {}

		void operator=(const DT& rhs) {
			Index ind = op.getStart();
			do {
				op(ind)[component] = rhs;
			} while(next_index(op.getStart(), op.getStop(), ind));
		}

		void operator=(const Array<DT, Dim>& rhs) {
			Index ind = op.getStart();
			do {
				op(ind)[component] = rhs(ind);
			} while(next_index(op.getStart(), op.getStop(), ind));
		}

		constexpr DT operator()(const Index ind) const {
			return(op(ind)[component]);
		}

		constexpr Index getStart() const {
			return(op.getStart());
		}

		constexpr int getStart(int i) const {
			return(op.getStart(i));
		}

		constexpr Index getStop() const {
			return(op.getStop());
		}

		constexpr int getStop(int i) const {
			return(op.getStop(i));
		}
};

template<typename DataType, int Dimensionality, int Components>
struct is_arr<ArrayComp<DataType, Dimensionality, Components>> {
	  static const bool value = true;
};

template<typename DataType, int Dimensionality, int Components>
constexpr ArrayComp<DataType, Dimensionality, Components> comp(Array<TinyVector<DataType, Components>, Dimensionality>& arr, int component) {
	return(ArrayComp<DataType, Dimensionality, Components>(arr, component));
}

}

#endif
