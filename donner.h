/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_H
#define DONNER_H

#include <array>
#include <cassert>
#include <complex>
#include <functional>
#include <iostream>
#include <limits>
#include <ostream>
#include <typeinfo>
#include <type_traits>
#include <utility>
#include <vector>

#include "element-op.h"
#include "tinyvec.h"
#include "tinyvec-exp.h"
#include "tinyvec-op.h"
#include "indexing.h"
#include "array.h"
#include "array-exp.h"
#include "array-op.h"
#include "array-trafo.h"

#endif
