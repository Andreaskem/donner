/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_TINYVEC_EXP_H
#define DONNER_TINYVEC_EXP_H

#include "donner.h"

namespace donner {

/*
 * -------------------------------------------------
 * Helpers
 * -------------------------------------------------
 */

template<typename Operand>
constexpr Operand eval_for_vecs(const Operand& op, int, typename std::enable_if<!is_vec<Operand>::value>::type* = nullptr) {
	return(op);
}

template<typename Operand>
constexpr typename Operand::DT eval_for_vecs(const Operand& op, int i, typename std::enable_if<is_vec<Operand>::value>::type* = nullptr) {
	return(op(i));
}

template<typename FirstOperand, typename SecondOperand, typename Operation>
class VectorExpression {
	typedef VectorOperand<FirstOperand, SecondOperand> MainOperand;

	const FirstOperand& op1;
	const SecondOperand& op2;

	public:
		typedef typename MainOperand::DT DT;
		static const int Size = MainOperand::Size;

		constexpr VectorExpression(const FirstOperand& op1, const SecondOperand& op2) :
			op1(op1), op2(op2) {}

		constexpr DT operator()(int i) const {
			return(Operation::evaluate(eval_for_vecs(op1, i), eval_for_vecs(op2, i)));
		}

		constexpr DT operator[](int i) const {
			return(Operation::evaluate(eval_for_vecs(op1, i), eval_for_vecs(op2, i)));
		}
};

template<typename FirstOperand, typename SecondOperand, typename Operation>
struct is_vec<VectorExpression<FirstOperand, SecondOperand, Operation>> {
	  static const bool value = true;
};

}

#endif
