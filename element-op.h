/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_ELEMENT_OP_H
#define DONNER_ELEMENT_OP_H

#include "donner.h"

namespace donner {

template<typename T, typename Enable = void>
struct base_type {
	typedef T DT;
};

template<typename T>
struct is_vec {
	static const bool value = false;
};

template<typename T>
struct is_arr {
	static const bool value = false;
};

template<typename FirstOperand, typename SecondOperand>
struct ElementSum {
	typedef typename std::common_type<FirstOperand, SecondOperand>::type DT;
	static constexpr DT evaluate(const FirstOperand& op1, const SecondOperand& op2) {
		return(op1 + op2);
	}
};

template<typename FirstOperand, typename SecondOperand>
struct ElementDifference {
	typedef typename std::common_type<FirstOperand, SecondOperand>::type DT;
	static constexpr DT evaluate(const FirstOperand& op1, const SecondOperand& op2) {
		return(op1 - op2);
	}
};

template<typename FirstOperand, typename SecondOperand>
struct ElementProduct {
	typedef typename std::common_type<FirstOperand, SecondOperand>::type DT;
	static constexpr DT evaluate(const FirstOperand& op1, const SecondOperand& op2) {
		return(op1 * op2);
	}
};

template<typename FirstOperand, typename SecondOperand>
struct ElementQuotient {
	typedef typename std::common_type<FirstOperand, SecondOperand>::type DT;
	static constexpr DT evaluate(const FirstOperand& op1, const SecondOperand& op2) {
		return(op1 / op2);
	}
};

template<typename FirstOperand, typename SecondOperand>
struct ElementModulo {
	typedef typename std::common_type<FirstOperand, SecondOperand>::type DT;
	static constexpr DT evaluate(const FirstOperand& op1, const SecondOperand& op2) {
		return(op1 % op2);
	}
};

}

#endif
