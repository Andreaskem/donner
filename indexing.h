/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_INDEXING_H
#define DONNER_INDEXING_H

#include "donner.h"

namespace donner {

static const int fromStart = std::numeric_limits<int>::min();
static const int toEnd = std::numeric_limits<int>::max();

struct Range {
	int start;
	int stop;
	int stride;

	constexpr Range(int start, int stop, int stride) : start(start), stop(stop), stride(stride) {}
	constexpr Range(const Range& rhs) : start(rhs.start), stop(rhs.stop), stride(rhs.stride) {}
	constexpr Range(int start, int stop) : start(start), stop(stop), stride(1) {}
	constexpr Range(int start_stop) : start(start_stop), stop(start_stop), stride(1) {}
	constexpr Range() : start(0), stop(0), stride(1) {}

	static Range all() {
		return(Range(fromStart, toEnd));
	}
};

template<int Dimensionality, bool RowMajor = true>
struct Indexer;

template<int Dimensionality>
struct Indexer<Dimensionality, true> {
	inline static int index(const TinyVector<int, Dimensionality>& shape, const TinyVector<int, Dimensionality>& indices) {
		int index = 0;

		for(int k = 1; k <= Dimensionality; ++k) {
			int inner = 1;
			for(int l = k+1; l <= Dimensionality; ++l) {
				inner *= shape[l-1];
			}
			index += inner * indices[k-1];
		}

		return(index);
	}
};

template<int Dimensionality>
struct Indexer<Dimensionality, false> {
	inline static int index(const TinyVector<int, Dimensionality>& shape, const TinyVector<int, Dimensionality>& indices) {
		int index = 0;

		for(int k = 1; k <= Dimensionality; ++k) {
			int inner = 1;
			for(int l = k+1; l <= Dimensionality; ++l) {
				inner *= shape[l-1];
			}
			index += inner * indices[k-1];
		}

		return(index);
	}
};

template<int Dimensionality>
inline TinyVector<int, Dimensionality> get_offsets(const TinyVector<int, Dimensionality>& shape) {
	TinyVector<int, Dimensionality> ret;

	TinyVector<int, Dimensionality> ind;
	for(int i = 0; i < Dimensionality; i++) {
		ind = 0;
		ret[i] = -Indexer<Dimensionality, true>::index(shape, ind);
		ind[i] = 1;
		ret[i] += Indexer<Dimensionality, true>::index(shape, ind);
	}

	return(ret);
}

template<int Dimensionality>
inline bool next_index(const TinyVector<int, Dimensionality>& start, const TinyVector<int, Dimensionality>& stop, TinyVector<int, Dimensionality>& ind) {
	int i = Dimensionality-1;
	++ind[i];

	while(ind[i] > stop[i]) {
		if(i == 0) {
			return(false);
		}
		ind[i] = start[i];
		--i;
		++ind[i];
	}

	return(true);
}

template<int Dimensionality>
inline bool next_index(int, int, int& ind) {
	++ind;
	if(ind == Dimensionality)
		return false;
	else
		return true;
}

template<int Dimensionality>
constexpr int static_index(const TinyVector<int, Dimensionality>& strides, const TinyVector<int, Dimensionality>& ind) {
	return(Dot<int, int, Dimensionality>::dot(strides, ind));
}

}

#endif
