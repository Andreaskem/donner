/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_ARRAY_OP_H
#define DONNER_ARRAY_OP_H

#include "donner.h"

namespace donner {

/*
 * -------------------------------------------------
 * Sums
 * -------------------------------------------------
 */

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_arr<FirstOperand>::value || is_arr<SecondOperand>::value>::type* = nullptr>
constexpr ArrayExpression<FirstOperand, SecondOperand, ElementSum<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator+(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

/*
 * -------------------------------------------------
 * Differences
 * -------------------------------------------------
 */

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_arr<FirstOperand>::value || is_arr<SecondOperand>::value>::type* = nullptr>
constexpr ArrayExpression<FirstOperand, SecondOperand, ElementDifference<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator-(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

/*
 * -------------------------------------------------
 * Products
 * -------------------------------------------------
 */

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_arr<FirstOperand>::value || is_arr<SecondOperand>::value>::type* = nullptr>
constexpr ArrayExpression<FirstOperand, SecondOperand, ElementProduct<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator*(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

/*
 * -------------------------------------------------
 * Quotients
 * -------------------------------------------------
 */

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_arr<FirstOperand>::value || is_arr<SecondOperand>::value>::type* = nullptr>
constexpr ArrayExpression<FirstOperand, SecondOperand, ElementQuotient<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator/(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

/*
 * -------------------------------------------------
 * Modulo
 * -------------------------------------------------
 */

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_arr<FirstOperand>::value || is_arr<SecondOperand>::value>::type* = nullptr>
constexpr ArrayExpression<FirstOperand, SecondOperand, ElementModulo<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator%(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

/*
 * -------------------------------------------------
 * Output
 * -------------------------------------------------
 */

template<typename Arr, typename std::enable_if<is_arr<Arr>::value && Arr::Dim == 1>::type* = nullptr>
std::ostream& operator<<(std::ostream& os, const Arr& obj) {
	os << "A" << typeid(typename Arr::DT).name() << " [" << obj.getStart()[0] << ", " << obj.getStop()[0] << "] " << "( ";
	for(int i = obj.getStart()[0]; i <= obj.getStop()[0]-1; ++i) {
		os << obj(i) << ", ";
	}
	os << obj(obj.getStop());
	os << " )";
	return os;
}

template<typename Arr, typename std::enable_if<is_arr<Arr>::value && Arr::Dim == 2>::type* = nullptr>
std::ostream& operator<<(std::ostream& os, const Arr& obj) {
	os << "A" << typeid(typename Arr::DT).name() << " " << obj.getStart() << "x" << obj.getStop() << "\n";
	for(int i = obj.getStart()[0]; i <= obj.getStop()[0]; ++i) {
		os << "|";
		for(int j = obj.getStart()[1]; j <= obj.getStop()[1]-1; ++j) {
			os << obj(TinyVector<int, 2>{i, j}) << ", ";
		}
		os << obj(TinyVector<int, 2>{i, obj.getStop()[1]});
		os << "|\n";
	}
	return os;
}

}

#endif
