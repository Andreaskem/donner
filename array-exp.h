/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_ARRAY_EXP_H
#define DONNER_ARRAY_EXP_H

#include "donner.h"

namespace donner {

/*
 * -------------------------------------------------
 * Helpers
 * -------------------------------------------------
 */

template<typename Operand, int Dimensionality>
constexpr Operand eval_for_arrs(const Operand& op, const TinyVector<int, Dimensionality>&, typename std::enable_if<!is_arr<Operand>::value>::type* = nullptr) {
	return(op);
}

template<typename Operand, int Dimensionality>
constexpr typename Operand::DT eval_for_arrs(const Operand& op, const TinyVector<int, Dimensionality>& ind, typename std::enable_if<is_arr<Operand>::value>::type* = nullptr) {
	return(op(ind));
}

template<typename Operand>
constexpr int start_for_arrs(const Operand&, int, typename std::enable_if<!is_arr<Operand>::value>::type* = nullptr) {
	return(std::numeric_limits<int>::min());
}

template<typename Operand>
constexpr int start_for_arrs(const Operand& op, int i, typename std::enable_if<is_arr<Operand>::value>::type* = nullptr) {
	return(op.getStart(i));
}

template<typename Operand>
constexpr int stop_for_arrs(const Operand&, int, typename std::enable_if<!is_arr<Operand>::value>::type* = nullptr) {
	return(std::numeric_limits<int>::max());
}

template<typename Operand>
constexpr int stop_for_arrs(const Operand& op, int i, typename std::enable_if<is_arr<Operand>::value>::type* = nullptr) {
	return(op.getStop(i));
}

template<typename FirstOperand, typename SecondOperand, typename Operation>
class ArrayExpression {
	public:
		typedef ArrayOperand<FirstOperand, SecondOperand> MainOperand;
		typedef typename MainOperand::DT DT;
		static const int Dim = MainOperand::Dim;
		typedef TinyVector<int, Dim> Index;

	private:
		const FirstOperand& op1;
		const SecondOperand& op2;

	public:
		constexpr ArrayExpression(const FirstOperand& op1, const SecondOperand& op2) : op1(op1), op2(op2) {}

		constexpr DT operator()(const Index& ind) const {
			return(Operation::evaluate(eval_for_arrs(op1, ind), eval_for_arrs(op2, ind)));
		}

		inline Index getStart() const {
			TinyVector<int, Dim> start_ret;

			for(int i = 0; i < Dim; ++i) {
				start_ret[i] = getStart(i);
			}

			return(start_ret);
		}

		constexpr int getStart(int i) const {
			return(std::max(start_for_arrs(op1, i), start_for_arrs(op2, i)));
		}

		inline Index getStop() const {
			TinyVector<int, Dim> stop_ret;

			for(int i = 0; i < Dim; ++i) {
				stop_ret[i] = getStop(i);
			}

			return(stop_ret);
		}

		constexpr int getStop(int i) const {
			return(std::min(stop_for_arrs(op1, i), stop_for_arrs(op2, i)));
		}
};

template<typename FirstOperand, typename SecondOperand, typename Operation>
struct is_arr<ArrayExpression<FirstOperand, SecondOperand, Operation>> {
	  static const bool value = true;
};

template<typename Operand, typename Operation>
class ArrayUnaryExpression {
	public:
		typedef typename Operand::DT DT;
		static const int Dim = Operand::Dim;
		typedef TinyVector<int, Dim> Index;

	private:
		const Operand& op;
		const Operation f;

	public:
		constexpr ArrayUnaryExpression(const Operand& op, const Operation& f) : op(op), f(f) {}

		constexpr DT operator()(const Index& ind) const {
			return(f(op(ind)));
		}

		constexpr Index getStart() const {
			return(op.getStart());
		}

		constexpr int getStart(int i) const {
			return(op.getStart(i));
		}

		constexpr Index getStop() const {
			return(op.getStop());
		}

		constexpr int getStop(int i) const {
			return(op.getStop(i));
		}
};

template<typename Operand, typename Operation>
struct is_arr<ArrayUnaryExpression<Operand, Operation>> {
	  static const bool value = true;
};

template<typename Operand, typename Operation>
constexpr ArrayUnaryExpression<Operand, Operation> apply(const Operand& op, const Operation& f, typename std::enable_if<is_arr<Operand>::value>::type* = nullptr) {
	return{op, f};
}

}

#endif
