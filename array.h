/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_ARRAY_H
#define DONNER_ARRAY_H

#include "donner.h"

namespace donner {

typedef int StorageSpecifier;
static const int Value = 0;
static const int Reference = 1;
static const int CReference = 2;

template<typename FirstOperand, typename SecondOperand>
using ArrayOperand = typename std::conditional<is_arr<FirstOperand>::value, FirstOperand, SecondOperand>::type;

template<typename DataType, int Dimensionality, StorageSpecifier OwnData = Value>
class Array {
	template<typename, int, StorageSpecifier>
	friend class Array;

	public:
		typedef DataType DT;
		static const int Dim = Dimensionality;

		typedef TinyVector<int, Dim> Index;
		typedef Array<DT, Dim, OwnData> A;
		typedef Array<DT, Dim, Value> VA;
		typedef Array<DT, Dim, Reference> RA;
		typedef Array<DT, Dim, CReference> CRA;

	private:
		typedef typename std::conditional<
			OwnData == Value, std::vector<DT>, typename std::conditional<
				OwnData == Reference, std::vector<DT>&, const std::vector<DT>&
			>::type
		>::type StorageType;

		Index _shape;
		Index base;
		Index offsets;
		int base_offset;
		StorageType storage;

		Index start;
		Index stop;

		constexpr int index(const Index& indices) const {
			return(static_index(offsets, indices) - base_offset);
		}
	public:
		inline bool next(Index& ind) const {
			int i = Dim-1;
			++ind[i];

			while(ind[i] > stop[i]) {
				if(i == 0) {
					return(false);
				}
				ind[i] = start[i];
				--i;
				++ind[i];
			}

			return(true);
		}

	public:
		constexpr Array() {}

		constexpr Array(const Index& shape) : _shape(shape), base(0), offsets(get_offsets(shape)), base_offset(dot(base, offsets)), storage(product(shape)), start(0), stop(shape-1) {}

		template<typename... Extents>
		constexpr Array(int extent, Extents... extents) : Array(Index{extent, extents...}) {}

		template<StorageSpecifier RhsOwnData>
		constexpr Array(const Array<DT, Dim, RhsOwnData>& rhs) : _shape(rhs._shape), base(rhs.base), offsets(rhs.offsets), base_offset(rhs.base_offset), storage(rhs.storage), start(rhs.getStart()), stop(rhs.getStop()) {}

		constexpr Array(const Index& start, const Index& shape) : _shape(shape), base(start), offsets(get_offsets(shape)), base_offset(dot(base, offsets)), storage(product(shape)), start(start), stop(start+shape-1) {}

		template<StorageSpecifier RhsOwnData>
		Array(const Array<DT, Dim, RhsOwnData>& rhs, const TinyVector<Range, Dim>& ranges) : _shape(rhs._shape), base(rhs.base), offsets(rhs.offsets), base_offset(rhs.base_offset), storage(rhs.storage) {
			for(int i = 0; i < Dim; ++i) {
				start[i] = (ranges[i].start == fromStart) ? base[i] : ranges[i].start;
				stop[i] = (ranges[i].stop == toEnd) ? (base[i] + _shape[i] - 1) : ranges[i].stop;
			}
		}

		template<StorageSpecifier RhsOwnData>
		Array(Array<DT, Dim, RhsOwnData>& rhs, const TinyVector<Range, Dim>& ranges) : _shape(rhs._shape), base(rhs.base), offsets(rhs.offsets), base_offset(rhs.base_offset), storage(rhs.storage) {
			for(int i = 0; i < Dim; ++i) {
				start[i] = (ranges[i].start == fromStart) ? base[i] : ranges[i].start;
				stop[i] = (ranges[i].stop == toEnd) ? (base[i] + _shape[i] - 1) : ranges[i].stop;
			}
		}

		template<StorageSpecifier S>
		void copy(const Array<DT, Dim, S>& rhs) {
			storage = rhs.storage;
			for(int i = 0; i < Dim; ++i) {
				start[i] = rhs.start[i];
				stop[i] = rhs.stop[i];
			}
		}

		constexpr const DT& operator()(const Index& ind) const {
			return(storage[index(ind)]);
		}

		template<typename... Indices>
		constexpr const DT& operator()(int index, Indices... indices) const {
			static_assert(sizeof...(indices)+1 == Dim, "Not enough indices for indexing");
			return((*this)(Index{index, indices...}));
		}

		inline DT& operator()(const Index& ind) {
			return(storage[index(ind)]);
		}

		template<typename... Indices>
		inline DT& operator()(int index, Indices... indices) {
			static_assert(sizeof...(indices)+1 == Dim, "Not enough indices for indexing");
			return((*this)(Index{index, indices...}));
		}

		inline RA operator()(const TinyVector<Range, Dim>& ranges) {
			return(RA(*this, ranges));
		}

		template<typename... Ranges>
		inline RA operator()(Range range, Ranges... ranges) {
			static_assert(sizeof...(ranges)+1 == Dim, "Not enough ranges for sub-arry");
			return(RA(*this, TinyVector<Range, Dim>{range, ranges...}));
		}

		constexpr const CRA operator()(const TinyVector<Range, Dim>& ranges) const {
			return(CRA(*this, ranges));
		}

		template<typename... Ranges>
		constexpr const CRA operator()(Range range, Ranges... ranges) const {
			static_assert(sizeof...(ranges)+1 == Dim, "Not enough ranges for sub-arry");
			return(CRA(*this, TinyVector<Range, Dim>{range, ranges...}));
		}

		template<typename T, typename std::enable_if<is_arr<T>::value || is_vec<T>::value>::type* = nullptr>
		void operator=(const T& rhs) {
			Index lind = start;
			const typename T::Index rstart = rhs.getStart(), rstop = rhs.getStop();
			typename T::Index rind = rstart;
			do {
				(*this)(lind) = rhs(rind);
			} while(next_index<Dim>(start, stop, lind) && next_index<T::Dim>(rstart, rstop, rind));
		}

		template<typename T, typename std::enable_if<is_arr<T>::value || is_vec<T>::value>::type* = nullptr>
		void operator+=(const T& rhs) {
			Index lind = start;
			const typename T::Index rstart = rhs.getStart(), rstop = rhs.getStop();
			typename T::Index rind = rstart;
			do {
				(*this)(lind) += rhs(rind);
			} while(next_index<Dim>(start, stop, lind) && next_index<T::Dim>(rstart, rstop, rind));
		}

		template<typename T, typename std::enable_if<is_arr<T>::value || is_vec<T>::value>::type* = nullptr>
		void operator-=(const T& rhs) {
			Index lind = start;
			const typename T::Index rstart = rhs.getStart(), rstop = rhs.getStop();
			typename T::Index rind = rstart;
			do {
				(*this)(lind) -= rhs(rind);
			} while(next_index<Dim>(start, stop, lind) && next_index<T::Dim>(rstart, rstop, rind));
		}

		template<typename T, typename std::enable_if<is_arr<T>::value || is_vec<T>::value>::type* = nullptr>
		void operator*=(const T& rhs) {
			Index lind = start;
			const typename T::Index rstart = rhs.getStart(), rstop = rhs.getStop();
			typename T::Index rind = rstart;
			do {
				(*this)(lind) *= rhs(rind);
			} while(next_index<Dim>(start, stop, lind) && next_index<T::Dim>(rstart, rstop, rind));
		}

		template<typename T, typename std::enable_if<is_arr<T>::value || is_vec<T>::value>::type* = nullptr>
		void operator/=(const T& rhs) {
			Index lind = start;
			const typename T::Index rstart = rhs.getStart(), rstop = rhs.getStop();
			typename T::Index rind = rstart;
			do {
				(*this)(lind) /= rhs(rind);
			} while(next_index<Dim>(start, stop, lind) && next_index<T::Dim>(rstart, rstop, rind));
		}

		void operator=(const DT& rhs) {
			Index ind = start;
			do {
				(*this)(ind) = rhs;
			} while(next(ind));
		}

		void operator+=(const DT& rhs) {
			Index ind = start;
			do {
				(*this)(ind) += rhs;
			} while(next(ind));
		}

		void operator-=(const DT& rhs) {
			Index ind = start;
			do {
				(*this)(ind) -= rhs;
			} while(next(ind));
		}

		void operator*=(const DT& rhs) {
			Index ind = start;
			do {
				(*this)(ind) *= rhs;
			} while(next(ind));
		}

		void operator/=(const DT& rhs) {
			Index ind = start;
			do {
				(*this)(ind) /= rhs;
			} while(next(ind));
		}

		// Should probably be deactivated for pure views
		// Think about how to do that
		// Also think about what to do with start/stop/shape
		void reindexSelf(const Index& b) {
			start = b;
			stop = b + _shape - 1;
			base = b;
			base_offset = dot(base, offsets);
		}

		// Should probably be deactivated for pure views
		// Think about how to do that
		// Also think about what to do with start/stop/shape
		void resize(const Index& s) {
			_shape = s;
			storage.resize(product(_shape));
			offsets = get_offsets(_shape);
			base_offset = dot(base, offsets);
			stop = start + s - 1;
		}

		template<typename... Extents>
		void resize(int extent, Extents... extents) {
			resize(Index{extent, extents...});
		}

		constexpr Index getStart() const {
			return(start);
		}

		constexpr int getStart(int i) const {
			return(start[i]);
		}

		constexpr Index getStop() const {
			return(stop);
		}

		constexpr int getStop(int i) const {
			return(stop[i]);
		}

		constexpr Index shape() const {
			return(_shape);
		}

		DT* data() {
			return(storage.data());
		}

		constexpr const DT* data() const {
			return(storage.data());
		}

		constexpr int numElements() const {
			return(storage.size());
		}

		constexpr int extent(int i) const {
			return(_shape[i]);
		}
};

template<typename DataType, int Dimensionality, StorageSpecifier OwnData>
struct is_arr<Array<DataType, Dimensionality, OwnData>> {
	  static const bool value = true;
};

template<typename T>
struct base_type<T, typename std::enable_if<is_arr<T>::value>::type> {
	typedef typename T::DT DT;
};

template<typename DataType, int Dimensionality>
struct Helper {
	Array<DataType, Dimensionality, Value>& ref;
};

template<typename DataType, int Dimensionality, typename... Values>
void cycleArrays(Array<DataType, Dimensionality, Value>& A, Values&... values) {
	std::array<Helper<DataType, Dimensionality>, sizeof...(values)> arr{values...};

	Array<DataType, Dimensionality, Value> tmp = std::move(A);
	A = std::move(arr[0].ref);
	for(size_t i = 1; i < sizeof...(values); ++i) {
		arr[i-1].ref = std::move(arr[i].ref);
	}
	arr[sizeof...(values)-1].ref = std::move(tmp);
}

}

#endif
