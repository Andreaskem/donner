/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_TINYVEC_OP_H
#define DONNER_TINYVEC_OP_H

#include "donner.h"

namespace donner {

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_vec<FirstOperand>::value || is_vec<SecondOperand>::value>::type* = nullptr>
constexpr VectorExpression<FirstOperand, SecondOperand, ElementSum<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator+(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_vec<FirstOperand>::value || is_vec<SecondOperand>::value>::type* = nullptr>
constexpr VectorExpression<FirstOperand, SecondOperand, ElementDifference<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator-(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_vec<FirstOperand>::value || is_vec<SecondOperand>::value>::type* = nullptr>
constexpr VectorExpression<FirstOperand, SecondOperand, ElementProduct<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator*(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_vec<FirstOperand>::value || is_vec<SecondOperand>::value>::type* = nullptr>
constexpr VectorExpression<FirstOperand, SecondOperand, ElementQuotient<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator/(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

template<typename FirstOperand, typename SecondOperand, typename std::enable_if<is_vec<FirstOperand>::value || is_vec<SecondOperand>::value>::type* = nullptr>
constexpr VectorExpression<FirstOperand, SecondOperand, ElementModulo<typename base_type<FirstOperand>::DT, typename base_type<SecondOperand>::DT>> operator%(const FirstOperand& op1, const SecondOperand& op2) {
	return{op1, op2};
}

/*
 * -------------------------------------------------
 * Global Operators
 * -------------------------------------------------
 */

template<typename DataType, int DataSize>
TinyVector<bool, DataSize> operator==(const TinyVector<DataType, DataSize>& op1, const TinyVector<DataType, DataSize>& op2) {
	TinyVector<bool, DataSize> ret;
	for(int i = 0; i < DataSize; ++i) {
		ret[i] = (op1[i] == op2[i]);
	}
	return(ret);
}

template<typename DataType, int DataSize>
TinyVector<bool, DataSize> operator!=(const TinyVector<DataType, DataSize>& op1, const TinyVector<DataType, DataSize>& op2) {
	TinyVector<bool, DataSize> ret;
	for(int i = 0; i < DataSize; ++i) {
		ret[i] = (op1[i] != op2[i]);
	}
	return(ret);
}

template<typename DataType, int DataSize>
TinyVector<bool, DataSize> operator<=(const TinyVector<DataType, DataSize>& op1, const TinyVector<DataType, DataSize>& op2) {
	TinyVector<bool, DataSize> ret;
	for(int i = 0; i < DataSize; ++i) {
		ret[i] = (op1[i] <= op2[i]);
	}
	return(ret);
}

template<typename DataType, int DataSize>
TinyVector<bool, DataSize> operator>=(const TinyVector<DataType, DataSize>& op1, const TinyVector<DataType, DataSize>& op2) {
	TinyVector<bool, DataSize> ret;
	for(int i = 0; i < DataSize; ++i) {
		ret[i] = (op1[i] >= op2[i]);
	}
	return(ret);
}

template<typename DataType, int DataSize>
TinyVector<bool, DataSize> operator>(const TinyVector<DataType, DataSize>& op1, const TinyVector<DataType, DataSize>& op2) {
	TinyVector<bool, DataSize> ret;
	for(int i = 0; i < DataSize; ++i) {
		ret[i] = (op1[i] > op2[i]);
	}
	return(ret);
}

template<typename DataType, int DataSize>
TinyVector<bool, DataSize> operator<(const TinyVector<DataType, DataSize>& op1, const TinyVector<DataType, DataSize>& op2) {
	TinyVector<bool, DataSize> ret;
	for(int i = 0; i < DataSize; ++i) {
		ret[i] = (op1[i] < op2[i]);
	}
	return(ret);
}

/*
 * -------------------------------------------------
 * Global Functions
 * -------------------------------------------------
 */

template<typename DataType1, typename DataType2, int DataSize, int Count = DataSize>
struct Dot {
	static constexpr typename std::common_type<DataType1, DataType2>::type dot(const TinyVector<DataType1, DataSize>& v, const TinyVector<DataType2, DataSize>& w) {
		return(Dot<DataType1, DataType2, DataSize, Count-1>::dot(v, w) + v[Count-1] * w[Count-1]);
	}
};

template<typename DataType1, typename DataType2, int DataSize>
struct Dot<DataType1, DataType2, DataSize, 1> {
	static constexpr typename std::common_type<DataType1, DataType2>::type dot(const TinyVector<DataType1, DataSize>& v, const TinyVector<DataType2, DataSize>& w) {
		return(v[0] * w[0]);
	}
};

template<typename DataType1, typename DataType2, int DataSize>
constexpr typename std::common_type<DataType1, DataType2>::type dot(const TinyVector<DataType1, DataSize>& v, const TinyVector<DataType2, DataSize>& w) {
	return(Dot<DataType1, DataType2, DataSize>::dot(v, w));
}

template<typename DataType1, typename DataType2>
constexpr TinyVector<typename std::common_type<DataType1, DataType2>::type, 3> cross(const TinyVector<DataType1, 3>& v, const TinyVector<DataType2, 3>& w) {
	return(TinyVector<typename std::common_type<DataType1, DataType2>::type, 3>{v[1]*w[2] - w[2]*v[1], v[2]*w[0] - v[0]*w[2], v[0]*w[1] - v[1]*w[0]});
}

template<typename DataType, int DataSize, int Count = DataSize>
struct Product {
	static constexpr DataType product(const TinyVector<DataType, DataSize>& v) {
		return(Product<DataType, DataSize, Count-1>::product(v) * v[Count-1]);
	}
};

template<typename DataType, int DataSize>
struct Product<DataType, DataSize, 1> {
	static constexpr DataType product(const TinyVector<DataType, DataSize>& v) {
		return(v[0]);
	}
};

template<typename DataType, int DataSize>
constexpr DataType product(const TinyVector<DataType, DataSize>& v) {
	return(Product<DataType, DataSize>::product(v));
}

template<typename DataType, int DataSize>
DataType sum(const TinyVector<DataType, DataSize>& v) {
	DataType ret = v[0];
	for(int i = 1; i < DataSize; ++i) {
		ret += v[i];
	}
	return(ret);
}

template<typename DataType, int DataSize>
bool any(const TinyVector<DataType, DataSize>& v) {
	for(int i = 0; i < DataSize; ++i) {
		if(v[i])
			return(true);
	}
	return(false);
}

template<typename DataType, int DataSize>
bool all(const TinyVector<DataType, DataSize>& v) {
	for(int i = 0; i < DataSize; ++i) {
		if(!v[i])
			return(false);
	}
	return(true);
}

/*
 * -------------------------------------------------
 * Output
 * -------------------------------------------------
 */

template<typename Vec, typename std::enable_if<is_vec<Vec>::value>::type* = nullptr>
std::ostream& operator<<(std::ostream& os, const Vec& obj) {
	os << "V" << typeid(typename Vec::DT).name() << Vec::Size << " ( ";
	for(int i = 0; i < Vec::Size-1; ++i) {
		os << obj[i] << ", ";
	}
	os << obj[Vec::Size-1];
	os << " )";
	return os;
}

}

#endif
