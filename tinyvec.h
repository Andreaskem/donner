/*
   Copyright 2015, Andreas Kempf

   This file is part of donner++.

   donner++ is free software: you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published
   by the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   donner++ is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with donner++. If not, see
   <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifndef DONNER_TINYVEC_H
#define DONNER_TINYVEC_H

#include "donner.h"

namespace donner {

template<typename Vec, typename Enable = void, int... Values>
struct CVec;
template<typename DataType, int DataSize, typename Enable = void, int... Values>
struct Seq;

template<typename DataType, int DataSize>
struct TinyVector {
	typedef DataType DT;
	static const int Size = DataSize;
	static const int Dim = 1;

	typedef int Index;

	typedef TinyVector<DT, Size> TV;

	std::array<DT, Size> storage;

	constexpr TinyVector() {}

	constexpr TinyVector(const TV& rhs) : storage(rhs.storage) {}
	constexpr TinyVector(TV&& rhs) noexcept : storage(std::move(rhs.storage)) {}

	constexpr TinyVector(DT rhs) : TinyVector{CVec<TV>::cvec(rhs)} {}
	constexpr TinyVector(bool, DT rhs) : storage{rhs} {}

	template<typename T, typename... Values>
	explicit constexpr TinyVector(T value, Values... values) : storage{static_cast<DT>(value), static_cast<DT>(values)...} {
		static_assert(sizeof...(values)+1 == Size, "Not enough values for Vector initialization.");
	}

	template<typename Vec>
	TinyVector(const Vec& rhs, typename std::enable_if<is_vec<Vec>::value>::type* = 0) {
		for(int i = 0; i < Size; ++i) {
			storage[i] = rhs(i);
		}
	}

	TV& operator=(DT rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] = rhs;
		}
		return(*this);
	}

	TV& operator=(const TV& rhs) {
		storage = rhs.storage;
		return(*this);
	}

	TV& operator=(TV&& rhs) {
		storage = std::move(rhs.storage);
		return(*this);
	}

	TV operator-() const {
		TV ret;
		for(int i = 0; i < Size; ++i) {
			ret[i] = -storage[i];
		}
		return(ret);
	}

	TV& operator+=(TV& rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] += rhs.storage[i];
		}
		return(*this);
	}

	TV& operator-=(const TV& rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] -= rhs.storage[i];
		}
		return(*this);
	}

	TV& operator*=(const TV& rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] *= rhs.storage[i];
		}
		return(*this);
	}

	TV& operator/=(const TV& rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] /= rhs.storage[i];
		}
		return(*this);
	}

	TV& operator+=(DT rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] += rhs;
		}
		return(*this);
	}

	TV& operator-=(DT rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] -= rhs;
		}
		return(*this);
	}

	TV& operator*=(DT rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] *= rhs;
		}
		return(*this);
	}

	TV& operator/=(DT rhs) {
		for(int i = 0; i < Size; ++i) {
			storage[i] /= rhs;
		}
		return(*this);
	}

	const DT& operator[](int index) const {
		assert(index >= 0 && index < Size);
		return(storage[index]);
	}

	inline DT& operator[](int index) {
		assert(index >= 0 && index < Size);
		return(storage[index]);
	}

	const DT& operator()(int index) const {
		assert(index >= 0 && index < Size);
		return(storage[index]);
	}

	inline DT& operator()(int index) {
		assert(index >= 0 && index < Size);
		return(storage[index]);
	}

	DT* data() {
		return(storage.data());
	}

	constexpr int getStart() const {
		return(0);
	}

	constexpr int getStop() const {
		return(Size-1);
	}

	static constexpr TinyVector<DT, Size> zero() {
		return(CVec<TV>::cvec(static_cast<DT>(0)));
	}

	static constexpr TinyVector<DT, Size> one() {
		return(CVec<TV>::cvec(static_cast<DT>(1)));
	}

	static constexpr TinyVector<DT, Size> seq() {
		return(Seq<DT, Size>::seq());
	}
};

template<typename DataType, int DataSize>
struct is_vec<TinyVector<DataType, DataSize>> {
	  static const bool value = true;
};

template<typename T>
struct base_type<T, typename std::enable_if<is_vec<T>::value>::type> {
	typedef typename T::DT DT;
};

template<typename FirstOperand, typename SecondOperand>
using VectorOperand = typename std::conditional<is_vec<FirstOperand>::value, FirstOperand, SecondOperand>::type;

template<typename DataType, int DataSize, int... Values>
struct Seq<DataType, DataSize, typename std::enable_if<sizeof...(Values) == DataSize>::type, Values...> {
	static constexpr TinyVector<DataType, DataSize> seq() {
		return(TinyVector<DataType, DataSize>{Values...});
	}
};

template<typename DataType, int DataSize, int... Values>
struct Seq<DataType, DataSize, typename std::enable_if<sizeof...(Values) < DataSize>::type, Values...> {
	static constexpr TinyVector<DataType, DataSize> seq() {
		return(Seq<DataType, DataSize, void, DataSize-sizeof...(Values)-1, Values...>::seq());
	}
};

template<typename DataType>
constexpr DataType dummy(DataType val, int) {
	return(val);
}

template<typename Vec, int... Values>
struct CVec<Vec, typename std::enable_if<is_vec<Vec>::value && sizeof...(Values) == Vec::Size && Vec::Size == 1>::type, Values...> {
	static constexpr Vec cvec(typename Vec::DT op) {
		return(Vec{false, op});
	}
};

template<typename Vec, int... Values>
struct CVec<Vec, typename std::enable_if<is_vec<Vec>::value && sizeof...(Values) == Vec::Size && Vec::Size != 1>::type, Values...> {
	static constexpr Vec cvec(typename Vec::DT op) {
		return(Vec{dummy(op, Values)...});
	}
};

template<typename Vec, int... Values>
struct CVec<Vec, typename std::enable_if<is_vec<Vec>::value && sizeof...(Values) < Vec::Size>::type, Values...> {
	static constexpr Vec cvec(typename Vec::DT op) {
		return(CVec<Vec, void, 0, Values...>::cvec(op));
	}
};

}

#endif
